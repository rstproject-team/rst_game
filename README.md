# Setting up the project #

## How do I get set up? ##

* Clone/download the repository
* Download the latest maps and assets from the download section (Deprecated)
* Add the maps and assets to their respective folders in the project (Deprecated)
* Right-click on the RoadSafety.uproject and generate VS project files (use VS2015 or VS2017)
* Open the project in the Unreal Editor 4.16
* Check if the current game mode is set according to our Wiki
* Compile the project
* Run the game in the editor and see if all seems OK

## Deploying to Android ##

* Follow the [official instructions](https://docs.unrealengine.com/latest/INT/Platforms/Android/GettingStarted/1/index.html) for deploying to Android devices
* File -> Package Project -> Android -> Android (ASTC)
* Select the folder and run the Install\_RoadSafety\_Development-arm64-es2.bat file once the packaging is completed