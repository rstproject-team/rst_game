// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MapOverlay.generated.h"

/**
 * 
 */
UCLASS()
class ROADSAFETY_API UMapOverlay : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Map Overlay Widget")
	FString widgetName;
	
	/** Constructor */
	UMapOverlay(const FObjectInitializer& ObjectInitializer);
	
};
