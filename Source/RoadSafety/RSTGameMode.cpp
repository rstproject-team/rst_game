// Fill out your copyright notice in the Description page of Project Settings.

#include "RoadSafety.h"
#include "RSTGameMode.h"
#include "EgocentricPlayerController.h"
#include "UObject/ConstructorHelpers.h"


ARSTGameMode::ARSTGameMode() : AGameModeBase()
{
	// Use our custom PlayerController class
	PlayerControllerClass = AEgocentricPlayerController::StaticClass();

	// Use MainHUD blueprint as default HUD
	static ConstructorHelpers::FClassFinder<AActor> MainHUD(TEXT("/Game/Blueprints/UserInterface/MainHUD_BP"));
	if (MainHUD.Class != NULL)
	{
		HUDClass = MainHUD.Class;
	}

	// Set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<ACharacter> PlayerCharacterBPClass(TEXT("/Game/Blueprints/EgocentricPlayer_BP"));
	if (PlayerCharacterBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerCharacterBPClass.Class;
	}


	//GameStateClass = ACustomGameState::StaticClass();
	//ReplaySpectatorPlayerControllerClass = ACustomReplaySpectatorPlayerController::StaticClass();
	//SpectatorClass = ACustomSpectatorClass::StaticClass();
}
