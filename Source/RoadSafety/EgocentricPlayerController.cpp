// Fill out your copyright notice in the Description page of Project Settings.

#include "RoadSafety.h"
#include "EgocentricPlayerController.h"
#include "Blueprint/UserWidget.h"


//Constructor
/*
AEgocentricPlayerController::AEgocentricPlayerController()
{
	bShowMouseCursor = false;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
}
*/

void AEgocentricPlayerController::BeginPlay()
{
	Super::BeginPlay();
	
	if (wMapOverlay) // Check if the Asset is assigned in the blueprint.
	{
		// Create the widget and store it.
		mapOverlay = CreateWidget<UUserWidget>(this, wMapOverlay);
	
		//Show the Cursor.
		bShowMouseCursor = true;
	}
}

void AEgocentricPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);
	
	if (!isWidgetAddedToViewport)
	{
		isWidgetAddedToViewport = true;
	
		if (mapOverlay && !isMainMenu)
		{
			mapOverlay->AddToViewport();
		}
	}
}


/*
void AEgocentricPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	// Bind the flick (swipe) gesture
	InputComponent->BindAction("Teleport", IE_Pressed, this, &AEgocentricPlayerController::Teleport);

	// Bind the MouseYaw method to the yaw axis (x-axis)
	// Passes the float value "axis" into the MouseYaw binding
	InputComponent->BindAxis("MouseYaw", this, &AEgocentricPlayerController::MouseYaw);

	// Bind the MousePitch method to the pitch axis (y-axis)
	// Passes the float value "axis" into the MousePitch binding
	InputComponent->BindAxis("MousePitch", this, &AEgocentricPlayerController::MousePitch);


	InputComponent->BindAxis("MoveForward", this, &AEgocentricPlayerController::MoveForward);
	InputComponent->BindAxis("MoveSide", this, &AEgocentricPlayerController::MoveSide);
}

void AEgocentricPlayerController::MouseYaw(float axis)
{
	UE_LOG(LogTemp, Warning, TEXT("Mouse X!"))
	mouseInput.X = axis;
}

void AEgocentricPlayerController::MousePitch(float axis)
{
	UE_LOG(LogTemp, Warning, TEXT("Mouse Y!"))
	mouseInput.Y = axis;
}

void AEgocentricPlayerController::Teleport()
{
	UE_LOG(LogTemp, Warning, TEXT("TELEPORTING!"))

	// TODO: add the transition effect (Start Trek warp like)
}

void AEgocentricPlayerController::MoveForward(float axis)
{
	UE_LOG(LogTemp, Warning, TEXT("FORWARD!"))
}

void AEgocentricPlayerController::MoveSide(float axis)
{
	UE_LOG(LogTemp, Warning, TEXT("SIDE!"))
}
*/
