// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "CharacterPositionIndicator.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ROADSAFETY_API UCharacterPositionIndicator : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UCharacterPositionIndicator();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(
		float DeltaTime,
		ELevelTick TickType,
		FActorComponentTickFunction* ThisTickFunction
	) override;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = LiveMap)
	bool isLiveMapViewActive;

private:
	// Owner of the component
	AActor *owningActor = nullptr;
	FString owningActorName;

	USceneComponent *ownerRootComponent = nullptr;

	// Store GoogleVR data
	FVector devicePosition;
	FRotator deviceOrientation;
	FRotator indicatorNewOrientation;
	
};
