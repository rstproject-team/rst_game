// Fill out your copyright notice in the Description page of Project Settings.

#include "RoadSafety.h"
#include "CharacterPositionIndicator.h"
#include "Kismet/HeadMountedDisplayFunctionLibrary.h"


// Sets default values for this component's properties
UCharacterPositionIndicator::UCharacterPositionIndicator()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.
	// You can turn these features off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	isLiveMapViewActive = false;
	// ...
}


// Called when the game starts
void UCharacterPositionIndicator::BeginPlay()
{
	Super::BeginPlay();

	UHeadMountedDisplayFunctionLibrary::GetOrientationAndPosition(deviceOrientation, devicePosition);

	indicatorNewOrientation.Yaw = deviceOrientation.Yaw;
	indicatorNewOrientation.Pitch = -90.f;
	indicatorNewOrientation.Roll = 0.f;

	owningActor = GetOwner();

	if (!owningActor)
	{
		UE_LOG(LogTemp, Error, TEXT("Owner for one of the character indicators is missing..."));
	}
	
	ownerRootComponent = owningActor->GetRootComponent();
}


// Called every frame
void UCharacterPositionIndicator::TickComponent(
	float DeltaTime,
	ELevelTick TickType,
	FActorComponentTickFunction* ThisTickFunction
)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// Update the actor's rotation even when the live map view is active
	if (ownerRootComponent && isLiveMapViewActive)
	{
		UHeadMountedDisplayFunctionLibrary::GetOrientationAndPosition(deviceOrientation, devicePosition);
		indicatorNewOrientation.Yaw = deviceOrientation.Yaw;

		// Update only the yaw value of the actor's root component
		ownerRootComponent->SetRelativeRotation(indicatorNewOrientation);
	}
}
