// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "EgocentricPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class ROADSAFETY_API AEgocentricPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	/*
	AEgocentricPlayerController();
	*/

	// Note: using forward declaration because widget is not included in the header, therefore,
	// to prevent circular dependency. This is not needed if the Widget Class is included in the .h
 
	// Reference UMG Asset in the Editor
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
	TSubclassOf<class UUserWidget> wMapOverlay;
 
	// Variable to hold the widget After Creating it.
	UUserWidget* mapOverlay;
 
	// Override BeginPlay()
	void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Flags)
	bool isMainMenu = true;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Flags)
	bool isWidgetAddedToViewport = true;

	/*
	UPROPERTY()
	FVector2D mouseInput;
	*/

protected:
	/** Begin PlayerController interface */

	void PlayerTick(float DeltaTime) override;
	/*
	virtual void SetupInputComponent() override;
	*/

	/** End PlayerController interface */



	/*
	////////////////////////////////////////////////
	// Actions
	
	// Player will move by teleporting to a next safepoint
	UFUNCTION()
	void Teleport();

	// Handle mouse input
	UFUNCTION()
	void MouseYaw(float axis);

	UFUNCTION()
	void MousePitch(float axis);

	// Handles moving forward/backward
	UFUNCTION()
	void MoveForward(float Val);

	// Handles stafing movement, left and right
	UFUNCTION()
	void MoveSide(float Val);
	*/

};
