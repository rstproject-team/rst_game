// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "EgocentricPlayer.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnShowMap);

UCLASS()
class ROADSAFETY_API AEgocentricPlayer : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEgocentricPlayer();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Move in the direction the camera is facing
	UFUNCTION()
	void MoveForwardTouch(float speed = 1.f);

	// Player will move by walking or running
	UFUNCTION(BlueprintCallable, Category = "RSTPlayer")
	void WalkOrRun(bool isWalking);

	// Print the input data from the sensors on the screen during runtime
	UFUNCTION()
	void PrintSensorData();

	UFUNCTION()
	void DisplayMap();

	UFUNCTION()
	void LimitCameraPitch();

	// Store raw data
	FVector myTilt;
	FVector myGravity;
	FVector myRotation;
	FVector myAccel;

	// Store GoogleVR data
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Camera, meta = (AllowPrivateAccess = "true"))
	FVector devicePosition;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera, meta = (AllowPrivateAccess = "true"))
	FRotator deviceOrientation;

	FString platformType;

	bool isFirstTick;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	/** Handles moving forward/backward */
	void MoveForward(float Value);

	/** Handles stafing movement, left and right */
	void MoveSide(float Value);

	/** Handles keyboard controlled teleportation */
	void TeleportForward();
	

	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FirstPersonCameraComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MovementParams)
	float walkingSpeed = 0.4f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MovementParams)
	float runningSpeed = 2.0f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = MovementParams)
	float cameraPitchUpperLimit = 50.f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = MovementParams)
	float cameraPitchLowerLimit = -50.f;

	UPROPERTY(BlueprintAssignable)
	FOnShowMap onShowMap;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = MovementParams)
	float minWalkingSpeed = 0.1f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = MovementParams)
	float maxWalkingSpeed = 0.5f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = MovementParams)
	float minRunningSpeed = 0.6f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = MovementParams)
	float maxRunningSpeed = 3.0f;
};
