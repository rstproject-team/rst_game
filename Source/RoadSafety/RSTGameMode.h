// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RSTGameMode.generated.h"

/**
 * 
 */
UCLASS()
class ROADSAFETY_API ARSTGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	// Constructor
	ARSTGameMode();
	
};
