// Fill out your copyright notice in the Description page of Project Settings.

#include "RoadSafety.h"
#include "EgocentricPlayer.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/InputSettings.h"
#include "Kismet/HeadMountedDisplayFunctionLibrary.h"


// Sets default values
AEgocentricPlayer::AEgocentricPlayer()
{
	// Set this pawn to call Tick() every frame.
	// You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	platformType = UGameplayStatics::GetPlatformName();

	isFirstTick = true;

	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(15.f, 120.0f);

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->RelativeLocation = FVector(0.f, 0.f, 120.f); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;
}

// Called when the game starts or when spawned
void AEgocentricPlayer::BeginPlay()
{
	Super::BeginPlay();

	UHeadMountedDisplayFunctionLibrary::EnableHMD(false);
}

// Called every frame
void AEgocentricPlayer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	/*
	GEngine->AddOnScreenDebugMessage(-3, 0.05f, FColor::Green,
		FString::Printf(TEXT("Hello egocentric one."))
	);
	*/

	UHeadMountedDisplayFunctionLibrary::GetOrientationAndPosition(deviceOrientation, devicePosition);

	/*
	GEngine->AddOnScreenDebugMessage(-2, 0.05f, FColor::Magenta,
		FString::Printf(TEXT("Platform: %s\n\n"), *platformType)
	);

	GEngine->AddOnScreenDebugMessage(-10, 0.05f, FColor::Emerald,
		FString::Printf(TEXT("Player Orientation: %f, %f, %f\n"),
			deviceOrientation.Yaw,
			deviceOrientation.Pitch,
			deviceOrientation.Roll)
	);
	*/


	// Get the initial Yaw value for the raw data sensors and adjust the camera direction so that
	// the starting direction in the world corresponds to real North.
	// For some reason, the GetInputMotionState() doesn't work inside the BeginPlay()
	if (isFirstTick)
	{
		// Get the data from the mobile device's motion sensors and store it in corresponding vectors
		(GetWorld()->GetFirstPlayerController())->GetInputMotionState(
			myTilt,
			myRotation,
			myGravity,
			myAccel
		);

		float initialYaw = FMath::RadiansToDegrees(myTilt.Y);

		if (initialYaw >= 0)
		{
			UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition(deviceOrientation.Yaw - initialYaw);
		}
		else
		{
			UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition(deviceOrientation.Yaw + initialYaw);
		}

		isFirstTick = false;
	}

	/*
	DisplayMap();

	LimitCameraPitch();
	*/

	//PrintSensorData();
}


// Called to bind functionality to input
void AEgocentricPlayer::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	if (platformType == "Windows")
	{
		// set up game play key bindings
		check(PlayerInputComponent);

		PlayerInputComponent->BindAction("TeleportByKeyboard", IE_Pressed, this, &AEgocentricPlayer::TeleportForward);

		PlayerInputComponent->BindAxis("MoveForward", this, &AEgocentricPlayer::MoveForward);
		PlayerInputComponent->BindAxis("MoveSide", this, &AEgocentricPlayer::MoveSide);
	}
	else
	{
		//PlayerInputComponent->BindAction("Teleport", IE_Pressed, this, &AEgocentricPlayer::TeleportForward);
	}
}


void AEgocentricPlayer::PrintSensorData()
{
	GEngine->AddOnScreenDebugMessage(-21, 0.05f, FColor::Green, FString::Printf(TEXT("Tilt X: %f"), myTilt.X));
	GEngine->AddOnScreenDebugMessage(-20, 0.05f, FColor::Green, FString::Printf(TEXT("Tilt Y: %f"), myTilt.Y));
	GEngine->AddOnScreenDebugMessage(-19, 0.05f, FColor::Green, FString::Printf(TEXT("Tilt Z: %f\n\n"), myTilt.Z));

	GEngine->AddOnScreenDebugMessage(-18, 0.05f, FColor::Yellow, FString::Printf(TEXT("Rotation X: %f"), myRotation.X));
	GEngine->AddOnScreenDebugMessage(-17, 0.05f, FColor::Yellow, FString::Printf(TEXT("Rotation Y: %f"), myRotation.Y));
	GEngine->AddOnScreenDebugMessage(-16, 0.05f, FColor::Yellow, FString::Printf(TEXT("Rotation Z: %f\n\n"), myRotation.Z));

	GEngine->AddOnScreenDebugMessage(-15, 0.05f, FColor::Orange, FString::Printf(TEXT("Gravity X: %f"), myGravity.X));
	GEngine->AddOnScreenDebugMessage(-14, 0.05f, FColor::Orange, FString::Printf(TEXT("Gravity Y: %f"), myGravity.Y));
	GEngine->AddOnScreenDebugMessage(-13, 0.05f, FColor::Orange, FString::Printf(TEXT("Gravity Z: %f\n\n"), myGravity.Z));

	GEngine->AddOnScreenDebugMessage(-12, 0.05f, FColor::Red, FString::Printf(TEXT("Acceleration X: %f"), myAccel.X));
	GEngine->AddOnScreenDebugMessage(-10, 0.05f, FColor::Red, FString::Printf(TEXT("Acceleration Y: %f"), myAccel.Y));
	GEngine->AddOnScreenDebugMessage(-9, 0.05f, FColor::Red, FString::Printf(TEXT("Acceleration Z: %f\n\n"), myAccel.Z));
}


void AEgocentricPlayer::DisplayMap()
{
	/*if (deviceOrientation.Pitch <= -70.f)
	{
		GEngine->AddOnScreenDebugMessage(-110, 0.05f, FColor::Yellow,
			FString::Printf(TEXT("MAP!!! MAP!!! MAP!!! @ %f"), deviceOrientation.Pitch)
		);
	}*/
}


void AEgocentricPlayer::LimitCameraPitch()
{
	/*APlayerController *playerController = GetWorld()->GetFirstPlayerController();

	if (deviceOrientation.Pitch <= cameraPitchLowerLimit || deviceOrientation.Pitch >= cameraPitchUpperLimit)
	{
		// TODO: This doesn't seem to affect the device. On the desktop it works.
		playerController->ClientSetCameraFade(true, FColor::Black, FVector2D(1.f, 0.f), 1.f);
	}*/
}


void AEgocentricPlayer::MoveForwardTouch(float speed)
{
	// Add movement in the direction where the camera is facing
	AddMovementInput(GetActorForwardVector(), speed);

	/*
	GEngine->AddOnScreenDebugMessage(-110, 0.05f, FColor::Yellow,
		FString::Printf(TEXT("Moving speed: %f"), speed)
	);
	*/

	// TODO: make the camera go up and down to simulate walking experience
}


void AEgocentricPlayer::WalkOrRun(bool isWalking)
{
	if (isWalking)
	{
		if (walkingSpeed > maxWalkingSpeed)
		{
			walkingSpeed = maxWalkingSpeed;
		}
		else if (walkingSpeed < minWalkingSpeed)
		{
			walkingSpeed = minWalkingSpeed;
		}

		MoveForwardTouch(walkingSpeed);
	}
	else
	{
		if (runningSpeed > maxRunningSpeed)
		{
			runningSpeed = maxRunningSpeed;
		}
		else if (runningSpeed < minRunningSpeed)
		{
			runningSpeed = minRunningSpeed;
		}

		MoveForwardTouch(runningSpeed);
	}
}


void AEgocentricPlayer::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}


void AEgocentricPlayer::MoveSide(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}


void AEgocentricPlayer::TeleportForward()
{
	/*
	GEngine->AddOnScreenDebugMessage(-59, 0.05f, FColor::Green,
		FString::Printf(TEXT("Teleported."))
	);
	*/
}


